<?php
//List employee by salary
function listEmployees($employee) {
      echo "Name: {$employee['name']} <br>";
      echo "Employer: {$employee['employer']} <br>";
      echo "Location: {$employee['location']} <br>";
      echo "Salary: {$employee['salary']} <br><br>";    
  }
//List employee by salary
function listEmployeesBySalary($employee) {
  if ($employee['salary'] > 70000) {
    echo "Name: {$employee['name']} <br>";
    echo "Employer: {$employee['employer']} <br>";
    echo "Location: {$employee['location']} <br>";
    echo "Salary: {$employee['salary']} <br><br>";
  }
}

//Function that lists and displays all employees living in a specific location
function listEmployeesByLoc($employee) {
    if ($employee['location'] == 'Berlin') {
      echo "Name: {$employee['name']} <br>";
      echo "Employer: {$employee['employer']} <br>";
      echo "Location: {$employee['location']} <br>";
      echo "Salary: {$employee['salary']} <br><br>";
    }
  }

//Function to find employee maximum salary
function findMaxSalary($employees) {
    $maxSalary = 0;
    $maxSalaryEmployee = [];

    foreach ($employees as $employee) {
        if ($employee['salary'] > $maxSalary) {
            $maxSalary = $employee['salary'];
            $maxSalaryEmployee = $employee;
        }
    }
    echo "Name: {$maxSalaryEmployee['name']} <br>";
    echo "Employer: {$maxSalaryEmployee['employer']} <br>";
    echo "Location: {$maxSalaryEmployee['location']} <br>";
    echo "Salary: {$maxSalaryEmployee['salary']} <br><br>";
    
}

//Function to find average salary
function findAverageSalary($employees) {
    $totalSalary = 0;
    $numEmployees = count($employees);
  
    foreach ($employees as $employee) {
      $totalSalary += $employee['salary'];
    }
  
    return $totalSalary / $numEmployees;
  }
  

$employees = [
  [
    "name" => "John Doe",
    "employer" => "Acme Inc.",
    "location" => "New York",
    "salary" => 75000
  ],
  [
    "name" => "Jane Smith",
    "employer" => "Acme Inc.",
    "location" => "Berlin",
    "salary" => 80000
  ],
  [
    "name" => "Bob Johnson",
    "employer" => "XYZ Corp.",
    "location" => "Belgrade",
    "salary" => 65000
  ],
  [
    "name" => "Sarah Williams",
    "employer" => "XYZ Corp.",
    "location" => "Berlin",
    "salary" => 72000
  ],
  [
    "name" => "Tom Brown",
    "employer" => "Acme Inc.",
    "location" => "Tokyo",
    "salary" => 78000
  ]
];

echo "<b>Employees List:</b> <br><br>";
array_walk($employees, 'listEmployees');

echo "<br><b>Employees with salary higher than 70000:</b> <br><br>";
array_walk($employees, 'listEmployeesBySalary');

echo "<br><b>Employees living in Berlin:</b> <br><br>";
array_walk($employees, 'listEmployeesByLoc');

echo "<br><b>Employees Having maximum salary:</b> <br><br>";
findMaxSalary($employees);

echo "<br><b>Employees average salary:".findAverageSalary($employees)."</b> <br><br>";
//echo findAverageSalary($employees);


?>

