<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
abstract class Fruit
{
    private $color;
    private $volume;

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function getVolume()
    {
        return $this->volume;
    }

    public function setVolume($volume)
    {
        $this->volume = $volume;
    }
}
////////////////////////////////////////////////////////

class Apple extends Fruit
{
    private $isWormy;

    public function getIsWormy()
    {
        return $this->isWormy;
    }

    public function setIsWormy($isWormy)
    {
        $this->isWormy = $isWormy;
    }
}

////////////////////////////////////////////////////////

interface FruitContainer
{
    public function addFruit(Fruit $fruit);
    public function getFruitCount();
    public function getRemainingSpace();
}

///////////////////////////////////////////////////////

class FruitContainerImp implements FruitContainer
{
    private $capacity;
    private $fruits;

    public function __construct($capacity)
    {
        $this->capacity = $capacity;
        $this->fruits = [];
    }

    public function addFruit(Fruit $fruit)
    {
        if (count($this->fruits) < $this->capacity) {
            array_push($this->fruits, $fruit);
        }
    }

    public function getFruitCount()
    {
        return count($this->fruits);
    }

    public function getRemainingSpace()
    {
        return $this->capacity - count($this->fruits);
    }

    public function getCapacity()
    {
        return $this->capacity;
    }

    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }

    public function getFruits()
    {
        return $this->fruits;
    }

    public function setFruits($fruits)
    {
        $this->fruits = $fruits;
    }
}
/////////////////////////////////////////

interface Strainer
{
    public function extractJuice(Fruit $fruit);
}

////////////////////////////////////////

class StrainerImp implements Strainer
{
    protected $juice;
    protected $juiceCount;

    public function extractJuice(Fruit $fruit)
    {
        $this->juice = $fruit->getVolume();
        $this->juiceCount += $this->juice;
    }

    public function getJuice()
    {
        return $this->juice;
    }

    public function getJuiceCount()
    {
        return $this->juiceCount;
    }

    public function setJuiceCount($juiceCount)
    {
        $this->juiceCount = $juiceCount;
    }

    public function setJuice($juice)
    {
        $this->juice  = $juice;
    }
}

////////////////////////////////////////

// Create instances of FruitContainerImp and StrainerImp
$container = new FruitContainerImp(100);//die("saaas");
$container->setCapacity(100);
$strainer = new StrainerImp();

// Set initial values for variables
$container->setFruits([]);
$strainer->setJuiceCount(0);

// Use a for loop to simulate the operation of the juicer for 100 actions
for ($i = 0; $i < 100; $i++) {
  // Add an apple to the container
  $apple = new Apple();
  $apple->setColor("Red");
  $apple->setVolume(100);
  $apple->setIsWormy(false);
  $container->addFruit($apple);

  // Extract juice from the apple
  $juice = $strainer->extractJuice($apple);
  $strainer->setJuice($juice);

  // Print the results of each action
  echo "Action $i:<br>";
  echo "Apple color: " . $apple->getColor() . "<br>";
  echo "Apple volume: " . $apple->getVolume() . "<br>";
  echo "Apple is wormy: " . ($apple->getIsWormy() ? "Yes" : "No") . "<br>";
  echo "Juice: " . $juice . "<br>";
  echo "Fruit count: " . $container->getFruitCount() . "<br>";
  echo "Remaining space: " . $container->getRemainingSpace() . "<br>";
  echo "Juice count: " . $strainer->getJuiceCount() . "<br><hr><br>";

  // Update the squeeze count
  $strainer->setJuiceCount($strainer->getJuiceCount() + 1);
}

// After the loop, print a message indicating that all actions have been completed
echo "All actions have been completed<br>";

?>

