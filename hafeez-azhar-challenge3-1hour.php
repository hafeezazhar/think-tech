<?php

// This function will return true if all the values of the given array are numeric
function is_numeric_array($array) {
    $is_numeric = true;
    array_walk($array, function ($value) use (&$is_numeric) {
        if (!is_numeric($value)) {
            $is_numeric = false;
        }
    });
    return $is_numeric;
}

// This function will return true if the input string contains only numeric values and comma/space separators
function is_valid_string($string) {
    return preg_match("/^[\d,\s]+$/", $string);
}

// Check if the request method is GET and if the input field is set and not empty
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['input_field']) && !empty($_GET['input_field'])) {
        // Remove whitespaces and commas from the start and end of the input string
        $str = str_replace([' ',','],',',trim($_GET['input_field'],','));

        // Convert the input string into an array of values
        $valueArray = explode(",",$str);

        // Check if all the values in the array are numeric
        if (is_numeric_array($valueArray)) {
            $evenArray = [];
            $oddArray = [];
            sort($valueArray);
           
            // Check if the input array has any values
            if(count($valueArray)) {
                foreach($valueArray as $value){
                    // Check if the value is even or odd
                    if ($value%2==0){
                        $evenArray[] = $value;
                    } else {
                        $oddArray[] = $value;
                    }
                }

            }
        } else {
            $error_message = 'Invalid input, please enter all numeric values.';
        }
    }
}

?>

<form action="" method="GET">
    <h2>Enter comma/space seperated numeric values:</h2>
  <input type="text" name="input_field" required style="width: 300px; height: 30px; font-size: 20px;">
  <input type="submit" value="Submit" style="width: 100px; height: 40px; font-size: 18px;">

  <!-- If the error message is set, display it -->
  <?php if (isset($error_message)) {
      echo '<p style="color: red;">' . $error_message . '</p>';
  } ?>

  <!-- If there are any even or odd numbers, display them -->
  <?php
    if(count($evenArray)){
        echo "<br><br><b><span style=\"background-color: lightgray; padding: 5px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>Even Number:</b>".implode(",",$evenArray); 
    }
    if(count($oddArray)){
        echo "<br><br><b><span style=\"background-color: powderblue; padding: 5px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>Odd Number:</b>".implode(",",$oddArray); 
    }
  ?> 
  

</form>


