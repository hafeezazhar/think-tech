<?php
// This function takes two arrays as input ($keys and $values)
function combineArrays($keys, $values) {
  $result = array(); // Declare an empty array to store the results.
  $index = 0; // Declare and initialize an index variable to keep track of the current position in the $values array.
  foreach ($keys as $key => $value) {
    // For each element in the $keys array, add a key-value pair to the $result array.
    $result[$value] = $values[$index];
    // Increment the index variable to point to the next value in the $values array.
    $index++;
  }

  // Return the resulting array.
  return $result;
}

// Declare two arrays for demonstration purposes.
$firstArray = ["field1" => "first", "field2" => "second", "field3" => "third"];
$secondArray = ["field1value" => "dinosaur", "field2value" => "pig", "field3value"=> "platypus"];

// Check if the two arrays have the same number of elements.
if(count($firstArray)!=count($secondArray)) {
  // If they don't, display an error message.
  echo "length should same";
} else {
  // If they do, call the combineArrays() function and store the result in a variable.
  $result = combineArrays($firstArray, array_values($secondArray));

  // Display the resulting array.
  echo "<pre><h2>First Array</h2>";
  print_r($firstArray);

  echo "<h2>Second Array</h2><pre>";
  print_r($secondArray);

  echo "<h2>Merged Array</h2><pre>";
  print_r($result);
}

?>
