<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin Panel</title>

    {{-- FontAwesome 5 kit --}}
    <script src="https://kit.fontawesome.com/85acf21687.js"></script>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body @if(Session::has('mode') && Session::get('mode')=='dark') class="dark-mode"
        @else
            class="light-mode"
        @endif   
    >   
    <div id="app">
        @include('layouts.adminNavbar')
        <div class="mb-5">
            @yield('content')
        </div>
    </div>
</body>
</html>
