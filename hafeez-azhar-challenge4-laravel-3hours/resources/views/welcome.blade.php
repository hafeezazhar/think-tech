<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>User Management System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        {{-- css --}}
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    </head>
    <body @if(Session::has('mode') && Session::get('mode')=='dark') class="dark-mode"
        @else
            class="light-mode"
        @endif   
    >   
        <div class="container-fluid text-center heading-custom">
            <h1 class="display-4" @if(Session::has('mode') && Session::get('mode')=='dark') style="color:white" @endif>Tech Cyber Users</h1>

            @if(Session::has('mode') && Session::get('mode')=='light')
                <a class="btn btn-primary" href="{{ route('mode-change',['mode'=>'dark']) }}">Dark Mode</a>
            @else
                <a class="btn btn-primary" href="{{ route('mode-change',['mode'=>'light']) }}">Light Mode</a>
            @endif
            <a class="btn btn-secondary " href="{{ route('login') }}">User/Admin Login</a>
            <a class="btn btn-primary" href="{{ route('register') }}">User Registration</a>
            
        </div>

        {{-- js --}}
        <link rel="stylesheet" href="{{ asset('js/app.js') }}">
        <link rel="stylesheet" href="{{ asset('js/style.js') }}">
    </body>
</html>
