<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LandingController extends Controller
{
    public function index() {
        
        if(Auth::guest()) {
            return view('welcome');
        }
        else {
            if(Auth::user()->admin == 1) {
                return redirect('/admin-panel');
            }
            else {
                return redirect('/home');
            }
        }
       
    }

     //Function to save mode in cookie
     public function modeChangeCookie(Request $request ){
        
        // Get the value to be stored from the request
        $value = $request->get('mode');

        // Store the value in a cookie for 60 minutes
        $minutes = 60;
        $response = new Response(redirect(\URL::previous()));
        $response->cookie('mode', $value, $minutes);
        return $response;
    }

    public function modeChange(Request $request ){
        \Session::put('mode', $request->get('mode'));
        //echo \Session::get('mode');
        //die("hafeez");
        return redirect()->back();

    }
}
