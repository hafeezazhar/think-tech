# Coding Challenge: Web Application 

Assignment by Think Tech Cyber

## Getting Started

1. Clone or download the repository
2. move into the project directory
3. Install laravel dependencies using `` composer install  ``
4. Install npm dependencies using `` npm install ``
5. Fill all the database information in the ".env" file
6. Generate app encryption key using `` php artisan key:generate ``
7. Generate the database tables using `` php artisan migrate `` 
8. **Important**: \
Generate Admin using `` db:seed `` \
email: "admin@admin.com", password: "password"
9. Run `` php artisan serve `` to open the project



